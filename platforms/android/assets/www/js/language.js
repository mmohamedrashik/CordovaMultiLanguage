var english     = 
		{   fname 			 : "First Name",
			lname  	  		 : "Last Name",
		    password  		 : "Password",
		    confirmpassword  : "Confirm Password",
		    mobile 			 : "Mobile Number",
		    submit			 :	"Submit",
		    samp			 :	"samp"};
	var malayalam = {
		    fname 	 		 : "പേരിന്റെ ആദ്യഭാഗം",
		    lname  	  		 : "പേരിന്റെ അവസാന ഭാഗം",
		    password  		 : "പാസ്വേഡ്",
		    confirmpassword  : "പാസ്വേഡ്  സ്ഥിരീകരിക്കുക",
		    mobile 			 : "മൊബൈൽ നമ്പർ",
		    submit 			 : "സമർപ്പിക്കുക",
		    samp 			 : "മാതൃക" 	
		};
	var hindi = {
		    fname 	 		 : "पहला नाम",
		    lname  	  		 : "अंतिम नाम",
		    password  		 : "पासवर्ड",
		    confirmpassword  : "पासवर्ड की पुष्टि कीजिये",
		    mobile 			 : "मोबाइल नंबर",
		    submit			 :  "जमा करें",
		    samp			 : "नमूना"	
		};

